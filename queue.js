let collection = [];

// Write the queue functions below.

// Output all the elements of the queue
function print() {
    return this.collection
    //true
  
}

// Adds element to the rear of the queue
function enqueue(element) {
    this.element = element
    collection.push(element)
    //this.collection = collection
    return this.collection
    //return collection
}


// Removes element from the front of the queue
function dequeue() {
    collection.shift()
    //collection.shift()
    //this.collection = collection
    return this.collection
    
}

// Show element at the front
function front() {
    return this.collection[0]
}

// Show total number of elements
function size() {
   return collection.length
   //true
}

// Outputs a Boolean value describing whether queue is empty or not
function isEmpty() {
    return collection.length === 0
    //true
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};